namespace dotes {

	class ConnectConfigString implements ConnectionConfigurationType {
		constructor(private name: string, private label: string) {
		}
		Name(): string {
			return this.name;
		}
		Label(): string {
			return this.label;
		}
		Render(f: HTMLFrameElement): void {
			f.innerText = "This should allow manipulation of a string";
		}
	};

	type HTTPMethod = "GET" | "HEAD" | "POST" | "PUT" | "DELETE" | "CONNECT" | "OPTIONS" | "TRACE" | "PATCH"

	interface fileStat {
		name: string
		path: string
		ext: string
		modTime:string
		size:number
		isDir:boolean
	}
	interface pingIn {}
	interface pingOut {
		time: string
	}
	interface httpError extends ConnectionError {
		status: number
		errorID: string
		error: string
		details?: string
	}
	interface projectInput {
		path: string
	}
	interface projectOutput {
		projectID: number
	}
	interface fileInput {
		content: Uint8Array
	}
	interface fileOutput {
		stat: fileStat
		content: string
	}
	interface treeOutput {
		stat:fileStat
		children: fileStat[]
	}

	const LocalServerConnection:ConnectionNewer = class LocalServerConnection implements Connection, ConnectionConfiguration {
		static UUID = "e3e8ba08-cae4-4261-882c-a5c41b783233"
		projectID: number
		initialConnection:Promise<number>

		constructor(private config: ConnectionConfig) {
		}

		static Create(n:ConnectionConfig):Promise<Connection> {
			var ret = new LocalServerConnection(n);
			return new Promise<Connection>((resolve, reject)=>{
				ret.Init().then((status)=>{
					if (status) {
						resolve(ret);
					} else {
						reject();
					}
				});
			});
		}


		private Init():Promise<boolean> {
			return this.send<projectInput, {projectID:number}>(
				"POST",
				"/project",
				{
					"path": this.config.root.path,
				}
			).then((d)=>{
				this.projectID = d.projectID;
				this.send<projectInput, {projectID:number}>(
					"POST",
					"/project/" + this.projectID.toString(),
					{
						"path": this.config.root.path,
					}
				);
				return true;
			}, ()=>{
				return false;
			});
		}

		Label():string {
			return this.config.label;
		}

		private send<Tin, Tout>(
			method: HTTPMethod,
			path: string,
			body: Tin
		): Promise<Tout> {
			return LocalServerConnection.send<Tin, Tout>(
				this.config, method, path, body
			);
		}

		private static send<Tin, Tout>(
			config: ConnectionConfig,
			method: HTTPMethod,
			path: string,
			body: Tin
		): Promise<Tout> {
			var bodyStr = "";
			if (body !== null) {
				bodyStr = JSON.stringify(body);
			}
			var xhttp = new XMLHttpRequest();
			xhttp.open(method, config.config.host + path, true);
			xhttp.setRequestHeader("Content-type", "application/json");
			xhttp.setRequestHeader("DotesServerToken", config.config.apiToken);
			xhttp.setRequestHeader("uuid", config.root.uuid);
			return new Promise<Tout>((resolve, reject) => {
				xhttp.onreadystatechange = function () {
					if (xhttp.readyState !== XMLHttpRequest.DONE) {
						return
					}
					let data:any;
					try {
						data = JSON.parse(xhttp.response);
					} catch (error) {
						console.error("Failed to parse JSON response");
						reject();
					}
					if (xhttp.status >= 200 && xhttp.status < 400) {
						resolve(data as Tout);
					} else {
						let e = data as httpError;
						let ret:ConnectionError = {
							error: e.error,
							errorID: e.errorID,
							details: e.details
						};
						reject(ret);
					}
				}
				xhttp.send(bodyStr);
			});
		}

		Ping(): ConnectionPromise<null> {
			return new Promise<null>((resolve, reject)=>{
				this.send<null, {}>(
					"GET",
					"/ping",
					null
				).then(()=>{
					resolve(null);
				});
			});
		}

		static Test(n: ConnectionConfig): ConnectionPromise<ConnectionTestResult> {
			return new Promise<ConnectionTestResult>((resolve, reject)=>{
				this.send<projectInput, {}>(
					n,
					"POST",
					"/project/check",
					{
						"path": n.root.path,
					}
				).then(()=>{
					resolve(PINGOK & CONFIGOK);
				}, ()=>{
					this.send<null, {}>(
						n,
						"GET",
						"/ping",
						null
					).then(()=>{
						resolve(PINGOK);
					},
					()=>{
						resolve(0);
					}
					)
				});
			});
		}

		GetFile(path:string): ConnectionPromise<Base64Data> {
			return new Promise<Base64Data>((resolve, reject)=>{
				this.send<null, fileOutput>(
					"GET",
					"/file/" + this.projectID + path,
					null
				).then((r)=>{
					resolve(new Base64Data(r.content));
				});
			});
		}
			

		WriteFile(path: string, content: Uint8Array): ConnectionPromise<null> {
			return new Promise<null>((resolve, reject)=>{
				this.send<fileInput, fileOutput>(
					"POST",
					"/file/" + this.projectID + path,
					{
						"content": content
					}
				).then((r)=>{
					resolve(null);
				});
			});
		}
		private static convertTime(s:string):Date {
			let d:Date = new Date();
			d.setFullYear(parseInt(s.substr(0,4)));
			d.setMonth(parseInt(s.substr(5,2)));
			d.setDate(parseInt(s.substr(8,2)));
			d.setUTCHours(parseInt(s.substr(11,2)));
			d.setMinutes(parseInt(s.substr(14,2)));
			d.setSeconds(parseInt(s.substr(17,2)));
			return d
		}
		private static convertStat(s:fileStat):ConnectionStat {
			return {
				isDir: s.isDir,
				modTime: LocalServerConnection.convertTime(s.modTime),
				name: s.name,
				path: s.path,
				size: s.size
			}
		}
		Stat(path:string):ConnectionPromise<ConnectionStat> {
			return new Promise<ConnectionStat>((resolve, reject)=>{
				this.send<null, fileStat>(
					"GET",
					"/stat/" + this.projectID + path,
					null
				).then((r)=>{
					resolve(LocalServerConnection.convertStat(r));
				});
			});
		}

		ListDirectory(path: string): ConnectionPromise<ConnectionStat[]> {
			return new Promise<ConnectionStat[]>((resolve, reject)=>{
				this.send<null, treeOutput>(
					"GET",
					"/tree/" + this.projectID + path,
					null
				).then((r)=>{
					let ret:ConnectionStat[] = [];
					(r.children||[]).forEach(c => {
						ret.push(LocalServerConnection.convertStat(c))
					});
					resolve(ret);
				});
			});
		}
		Mkdir(path: string): ConnectionPromise<null> {
			return new Promise<null>((resolve, reject)=>{
				this.send<null, null>(
					"PUT",
					"/tree/" + this.projectID + path,
					null
				).then((r)=>{
					resolve(null);
				});
			});
		}
		RemoveFile(path: string): ConnectionPromise<null> {
			return new Promise<null>((resolve, reject)=>{
				this.send<null, null>(
					"DELETE",
					"/file/" + this.projectID + path,
					null
				).then((r)=>{
					resolve(null);
				});
			});
		}
		RemoveDirectory(path: string): ConnectionPromise<null> {
			return new Promise<null>((resolve, reject)=>{
				this.send<null, null>(
					"DELETE",
					"/file/" + this.projectID + path,
					null
				).then((r)=>{
					resolve(null);
				});
			});
		}
		GetConfigurationTypes(): ConnectionConfigurationType[] {
			var ret: ConnectionConfigurationType[] = [];
			ret.push(new ConnectConfigString("host", "hostname"));
			ret.push(new ConnectConfigString("configPath", "configuration path on host"));
			ret.push(new ConnectConfigString("uuid", "UUID of configuration"));
			return ret;
		}
	}
	dotes.RegisterConnection(LocalServerConnection);

}
