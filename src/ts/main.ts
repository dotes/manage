namespace dotes {

interface Initializer {
    Init():void
}

export class Base64Data {
    constructor(public Data:string){}

    toString():string {
        return atob(this.Data);
    }
    toBytes():ArrayBuffer {
        let str = this.toString();
        var bytes = new Uint8Array(str.length);
        for (var i = 0; i < str.length; i++) {
            bytes[i] = str.charCodeAt(i);
        }
        return bytes.buffer;
    }
}

export function Init(page:string):Initializer {
    var ret:Initializer;
    switch (page) {
        case "config":
            ret = new ConfigInitializer();
            break;
        case "configRaw":
            ret = new ConfigRawInitializer();
            break;
        case "manager":
            ret = new Manager();
            break;
        default:
            throw("Unknown page " + page);
    }
    ret.Init();
    return ret;
}

class ConfigInitializer {
    Init() {
        let but = document.createElement("button");
        but.innerText = "add notebook";
        document.body.appendChild(but);
        
        let main = document.createElement("div");
        // let c = new ConfigurationManager(main);
        // but.onclick = c.AddNotebook;

        document.body.appendChild(main);

        // c.Init();

        // document.qu
    }
}

class ConfigRawInitializer {
    Init() {
        var ta = document.querySelector("#configraw textarea") as HTMLTextAreaElement;
        var notification = document.querySelector("#configraw .notification") as HTMLParagraphElement;

        ta.onkeyup = ()=>{
            var dataStr = ta.value
            var data:any;
            try{
                data = JSON.parse(dataStr)
            } catch(err) {
                notification.innerText = "Invalid JSON";
                notification.classList.add("invalid");
                return;
            }
            Configuration.Write(data as MinimalConfig);
            notification.innerText = "Wrote configuration to localstorage";
            notification.classList.remove("invalid");
        };
        var current_config = Configuration.Read();
        if (current_config !== null) {
            (document.querySelector("#configraw textarea") as HTMLTextAreaElement).value = JSON.stringify(current_config, null, 2);
        }
    }
}

class Article {
    private div:HTMLElement
    private createdUrls:string[]
    constructor(
        private connection:Connection,
        private path:string,
        private parent:HTMLElement,
        ){
            this.div = document.createElement("article");
            this.createdUrls = [];
        }

    Unload() {
        this.createdUrls.forEach((url)=>{
            URL.revokeObjectURL(url);
        })
        this.parent.removeChild(this.div);
    }

    async Render() {
        this.div.setAttribute("path", this.path);
        this.div.classList.add("loading");
        this.div.classList.add("note");
        let header = document.createElement("header");
        header.innerHTML = `<h1>${this.path}</h1>`;
        this.div.appendChild(header);
        this.parent.appendChild(this.div);

        // fetch file
        const data = await this.connection.GetFile(this.path);
        let markdown = data.toString();
        
        // be goofy and handle CR,CRLF,LF
        markdown = markdown.replace("\r\n", "\n");
        markdown = markdown.replace("\r", "\n");
        let lines = markdown.split("\n");

        // strip out frontmatter
        let frontmatter: string = "";
        let content: string = "";
        var inFrontmatter: boolean = false;
        lines.forEach((line, i) => {
            if (i == 0 && line == "---") {
                inFrontmatter = true;
                return;
            }
            if (inFrontmatter && line == "---") {
                inFrontmatter = false;
                return;
            }
            if (inFrontmatter) {
                frontmatter += line + "\n";
            }
            else {
                content += line + "\n";
            }
        });

        // convert yaml to json
        let front: any;
        try {
            front = jsyaml.load(frontmatter);
            console.log(front);
        }
        catch (error) {
            front = error;
        }

        // a bit cooky, but this holds images that are fetched and ready
        // to replace after the content is rendered
        var imagesToReplace:[string,Promise<string>][] = [];
        
        // render it
        let a = new marked.Renderer();
        a.heading = (text, level) => {
            return `<h${level + 1}>${text}</h${level + 1}>`;
        };
        let oldImage = a.image;
        a.image = (href, title, text) => {
            let path = href;
            if (href !== null && !(href.indexOf("://") > 0 || href.indexOf("//") === 0)) {
                // relative image...convert href to content url
                let contentType = 'image/png';
                if (href.search(/\.svg$/) > 0) {
                    contentType = "image/svg+xml";
                }
                let rel = this.path.substr(0,this.path.lastIndexOf('/')+1) + href;
                let tmpUrl = URL.createObjectURL(new Blob([], {type: contentType}));
                this.createdUrls.push(tmpUrl);
                imagesToReplace.push([tmpUrl, this.connection.GetFile(rel).then((data)=>{
                    let b = new Blob([data.toBytes()], {type: contentType});
                    let newUrl = URL.createObjectURL(b);
                    this.createdUrls.push(newUrl);
                    return newUrl;
                })]);
                href = tmpUrl;
            }
            if (title !== null) {
                return `<figure>
                <img src="${href}" title="${title||text||path}" alt="${text||path}">
                <figcaption>${title}</figcaption>
              </figure>`;
            }
            return `<figure>
            <img src="${href}" title="${text||path}" alt="${text||path}">
          </figure>`;
        };
        let oldCode = a.code;
        a.code = (code, language, isEscaped)=>{
            console.log(language);

            let preamble = "";
            if (language == "plotly" || language == 'plotly-json') {
                console.log(code);
                try {
                     // json definition is either just the data (an array!)
                    // or an object with 'data' and 'layout' properties
                    var data = JSON.parse(code);
                    if (!(typeof(data.length) !== 'number' && typeof(data.data) !== undefined)) {
                        data = {
                            data: data,
                            layout: {},
                        };
                    }
                    // TODO: trying to replace rather than recreate Plotly for performance
                    var newdiv = document.createElement('div');
                    Plotly.react(newdiv, data.data, data.layout, {
                        responsive: true,
                    });
                    return newdiv.outerHTML;
                } catch (error) {
                    let e = document.createElement("div");
                    e.classList.add('error')
                    e.classList.add('plotly-error');
                    e.innerText = error;
                    preamble = e.outerHTML;
                    language = 'json';
                }
            }
            let html = oldCode.call(a, code, language, isEscaped);
            let div = document.createElement('div');
            div.innerHTML = html;
            div.querySelectorAll('pre code').forEach((block:HTMLElement) => {
                hljs.highlightBlock(block);
            });
            return preamble + div.innerHTML;
        };
        // let oldCodeSpan = a.codespan;
        // a.codespan = (code)=>{
        //     return oldCodeSpan(code);
        // }

        let noteHTML = marked(content, {
            gfm: true,
            renderer: a
        });
        
        let main = document.createElement("main");
        let frontDisplay = document.createElement("pre");
        main.classList.add("note-content");
        main.innerHTML = noteHTML;
        this.div.appendChild(frontDisplay);
        frontDisplay.innerText = JSON.stringify(front, null, 2);
        renderMathInElement(main, {
            delimiters: [
                { left: "$$", right: "$$", display: true },
                { left: "$", right: "$", display: false },
            ]
        });
        this.div.appendChild(main);
        imagesToReplace.forEach((e)=>{
            e[1].then((newUrl)=>{
                main.querySelectorAll("[src]").forEach((el)=>{
                    if (el.getAttribute("src") == e[0]) {
                        el.setAttribute("src", newUrl);
                    }
                });
                URL.revokeObjectURL(e[0]);
            });
        });
    }
}

export class Manager {
    private connections:{[key:string]:Promise<Connection>}
    private articles:Article[]
  
    constructor() {
        this.connections = {};
        this.articles = [];
    }
  
    Init() {
        var current_config = Configuration.Read();
        if (current_config === null) {
            alert("no config found");
            return;
        }
        if (current_config.version  !== 1) {
            alert("invalid config version; upgrade not yet possible");
            return;
        }
        var c = current_config as Config;
        c.connections.forEach((e)=>{
            var connector = GetConnection(e.type);
            if (connector === null) {
                alert("requested unregistered connection");
                return;
            }
            this.connections[e.label]=connector.Create(e);
        });

        let nav = document.querySelector("nav.top") as HTMLElement;

        var THIS = this;
        c.connections.forEach((e, i)=>{
            let span = document.createElement("span");
            nav.appendChild(span);
            span.innerText = e.label;
            span.classList.add("noconnection");
            span.setAttribute("connectionLabel", e.label);
            this.connections[e.label].then((c)=>{
                let a = document.createElement("a");
                a.href = "#" + c.Label();
                a.innerText = c.Label();
                span.innerText = "";
                span.appendChild(a);
                a.onclick = function(){
                    window.location.hash = c.Label();
                    THIS.updateTree(c.Label());
                }
                span.classList.remove("noconnection");
                
            }).catch(()=>{
                var connector = GetConnection(e.type);
                if (connector !== null) {
                    connector.Test(e).then((r)=>{
                        if (r & PINGOK) {
                            span.classList.replace("noconnection", "invalid");
                        } else if (r & CONFIGOK) {
                            span.classList.replace("noconnection", "uninitialized");
                        }
                    })
                }
            });
        })

        // load current directory, if requested
        let url = new window.URL(document.URL);
        if (url.hash != "") {
            let s = url.hash.split("/");
            let root = s.shift();
            let path = s.join("/");
            if (root) {
                // hash starts with #
                this.updateTree(root.substr(1)).then(()=>{
                    if (path) {
                        this.updateDisplayWithDirectory("/" + path);
                    }
                });
            }
        }
    }

    private updateList(e:MouseEvent) {
        var t = e.target as HTMLElement;
        var path = t.getAttribute("path");
        if (path === null) {
            console.error("invalid path on dir element");
            return;
        }
        var i = (document.querySelector("#dirListing") as HTMLElement).getAttribute("connection");
        if (i ==  null) {
            console.error("invalid connection on dirListing")
            return;
        }
        if (t.getAttribute("open") !== null) {
            for (var j = t.children.length - 1; j >= 0; j--) {
                let child = t.children.item(j);
                if (child !== null && child.tagName == "OL") {
                    t.removeChild(child);
                }
            }
            t.removeAttribute("open");
            return;
        }
        var ppath = path;
        this.connections[i].then((c)=>{
            let ol = document.createElement("ol");
            t.appendChild(ol);
            t.setAttribute("open", "");
            c.ListDirectory(ppath).then((stats)=>{
                stats.forEach((s)=>{
                    this.setTreeItem(s, ol);
                });
            });
        });

    }

    private setTreeItem(s:ConnectionStat, parent:HTMLElement) {
        if (s.isDir) {
            let li = document.createElement("li");
            parent.appendChild(li);
            let span = document.createElement('span');
            span.innerText = s.name;
            li.appendChild(span);
            li.setAttribute("path", s.path);
            span.onclick = (e)=>{
                console.log("on dir " + s.path);
                e.stopPropagation();
                this.updateDisplayWithDirectory(s.path);
            }
            li.onclick = (e)=>{
                console.log("on li " + s.path);
                this.updateList(e);
                e.stopPropagation();
            }
        }
    }

    private async updateDisplayWithDirectory(d:string):Promise<any> {
        var sec = document.querySelector("main") as HTMLElement;
        var i = (document.querySelector("#dirListing") as HTMLElement).getAttribute("connection");
        if (i ==  null) {
            console.error("invalid connection on dirListing")
            return;
        }
        while (this.articles.length > 0) {
            let a = this.articles.shift();
            if (a) {
                a.Unload();
            }
        }
        return this.connections[i].then(async (c)=>{
            let ol = document.createElement("ol");
            const stats = await c.ListDirectory(d);
            return Promise.all(stats.reduce((a, s) => {
                if (!s.isDir && s.name.match(/\.md$/i)) {
                    let article = new Article(c, s.path, sec);
                    this.articles.push(article);
                    a.push(article.Render());
                }
                return a;
            }, [] as Promise<any>[]));
        });
    }

    private async updateTree(label:string):Promise<any> {
        var navTop = document.querySelector("nav.top") as HTMLElement;
        for (let i = 0; i < navTop.children.length; i++) {
            const child = navTop.children[i];
            if (child.getAttribute("connectionLabel") == label) {
                child.setAttribute("open", "");
            } else {
                child.removeAttribute("open");
            }
        }
        var nav = document.querySelector("nav.tree") as HTMLElement;
        nav.innerText = ""
        var listing = document.createElement("ol");
        listing.id = "dirListing";
        nav.appendChild(listing);
        listing.setAttribute("connection", label);
        const c = await this.connections[label];
        c.ListDirectory("").then((stats) => {
            stats.forEach((s) => {
                this.setTreeItem(s, listing);
            });
        });
    }
}

interface MinimalConfig {
    version: number
}

export interface ConnectionRootConfig {
    path: string
    uuid: string
}

export interface ConnectionConfig {
    type: string
    src: string|null
    label: string
    root: ConnectionRootConfig
    config: any
}

export interface Config extends MinimalConfig {
    connections: ConnectionConfig[]
}

export interface ConnectionPingResult {
    ok:boolean
    error:string|null
}

export interface ConnectionError {
    errorID?: string
    error: string
    details?: string
}

export interface ConnectionStat {
    name: string
    path: string
    isDir: boolean
    modTime: Date
    size: number
}

export interface ConnectionPromise<T> {
    then(onfulfilled?:(arg:T)=>any,
         onrejected?:(arg:ConnectionError)=>any):Promise<any>
    catch(onrejected?:(arg:ConnectionError)=>any):Promise<any>
}

export type ConnectionTestResult = number
export const PINGOK:ConnectionTestResult = 1;
export const CONFIGOK:ConnectionTestResult = 2;

export interface ConnectionNewer {
    UUID:string
    Create(n: ConnectionConfig):Promise<Connection>
    Test(n: ConnectionConfig):ConnectionPromise<ConnectionTestResult>
}

export interface Connection {
    Label():string

    WriteFile(path:string, content:Uint8Array):ConnectionPromise<null>
    GetFile(path:string):ConnectionPromise<Base64Data>
    Stat(path:string):ConnectionPromise<ConnectionStat>

    ListDirectory(path:string):ConnectionPromise<ConnectionStat[]>
    Mkdir(path:string):ConnectionPromise<null>
    RemoveFile(path:string):ConnectionPromise<null>
    RemoveDirectory(path:string):ConnectionPromise<null>
}


export interface ConnectionConfigurationType {
    Name(): string
    Label(): string
    Render(f:HTMLElement):void
}

export interface ConnectionConfiguration {
    GetConfigurationTypes(): ConnectionConfigurationType[]
}

class Configuration {
    static ReadRaw():string {
        return window.localStorage.getItem("dotes-manager-config") || "";
    }
    static Read():MinimalConfig|null {
        let raw = Configuration.ReadRaw()
        if (!!raw) {
            let data = JSON.parse(raw);
            if (data.version === undefined) {
                alert("Invalid dotes-manager-config storage value");
                return null;
            }
            return data as MinimalConfig
        }
        return null;
    }
    static Write(c:MinimalConfig) {
        window.localStorage.setItem("dotes-manager-config", JSON.stringify(c));
    }
}

}
