namespace dotes {
    interface registeredConnectionsI {
        [key:string]:ConnectionNewer
    }
    
    var registeredConnections:registeredConnectionsI = {};
    
    export function RegisterConnection(f:ConnectionNewer) {
        registeredConnections[f.UUID] = f;
    }

    export function GetConnection(uuid:string):ConnectionNewer|null {
        return registeredConnections[uuid] || null;
    }
}
